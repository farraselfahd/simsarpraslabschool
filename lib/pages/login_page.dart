import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
// import 'package:keyboard_visibility/keyboard_visibility.dart';

class LoginPage extends StatefulWidget {
  LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // _LoginPageState() {
  //   KeyboardVisibilityNotification().addNewListener(
  //     onChange: (bool visible) {
  //       setState(() {
  //         isKeyBoardVisible = visible;
  //       });
  //     },
  //   );
  // }
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _controllerEmail = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();
  bool _isKeyBoardVisible = false;
  bool _isObscured = false;

  @override
  void initState() {
    super.initState();

    _isObscured = true;
    // KeyboardVisibilityNotification().addNewListener(
    //   onChange: (bool visible) {
    //     setState(() {
    //       _isKeyBoardVisible = !_isKeyBoardVisible;
    //     });
    //   },
    // );
  }

  // Widget _pattern() {
  //   return Container(
  //     height: double.infinity,
  //     margin: EdgeInsets.symmetric(vertical: 30, horizontal: 0),
  //     decoration: BoxDecoration(
  //         image: DecorationImage(
  //             image: AssetImage("assets/images/vector.png"),
  //             fit: BoxFit.fitHeight)),
  //   );
  // }

  Widget _title() {
    return Container(
      // margin: EdgeInsets.symmetric(vertical: 140),
      margin: _isKeyBoardVisible
          ? EdgeInsets.symmetric(vertical: 30)
          : EdgeInsets.symmetric(vertical: 140),
      // alignment: Alignment.topCenter,
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text("Masuk",
                textAlign: TextAlign.left,
                style: GoogleFonts.poppins(
                    fontSize: 32, fontWeight: FontWeight.w600)),
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Silahkan masuk akun terlebih dahulu",
                textAlign: TextAlign.left,
                style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF686777)),
              ))
        ],
      ),
    );
  }

  Widget _emailField() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: SizedBox(
          // width: 328,
          child: TextFormField(
            // controller: _controllerEmail,
            decoration: InputDecoration(
              labelText: "Email",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12.0)),
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                print("KOSONG");
                return 'Silahkan masukkan email';
              }
              return null;
            },
          ),
        ));
  }

  Widget _passwordField() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: SizedBox(
          // width: 328,
          child: TextFormField(
            obscureText: _isObscured,
            // controller: _controllerPassword,
            decoration: InputDecoration(
                labelText: "Kata Sandi",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0)),
                suffixIcon: IconButton(
                    icon: _isObscured
                        ? Icon(Icons.visibility)
                        : Icon(Icons.visibility_off),
                    onPressed: (() {
                      setState(() {
                        _isObscured = !_isObscured;
                      });
                    }))),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Silahkan masukkan kata sandi';
              }
              return null;
            },
          ),
        ));
  }

  Widget _forgetPassword() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: TextButton(
            onPressed: () {},
            child: Text(
              "Lupa Kata Sandi ?",
              style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF554AF0)),
            )),
      ),
    );
  }

  Widget _form() {
    return Form(
      // alignment: Alignment.center,
      key: _formKey,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [_emailField(), _passwordField(), _forgetPassword()]),
    );
  }

  Widget _submitButton() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
          // width: 328,
          width: double.infinity,
          height: 56,
          child: ElevatedButton(
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ))),
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                // If the form is valid, display a snackbar. In the real world,
                // you'd often call a server or save the information in a database.
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Processing Data')),
                );
              }
            },
            child: Text(
              "Selesai",
              style: GoogleFonts.poppins(
                  fontSize: 16, fontWeight: FontWeight.w500),
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    _isKeyBoardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Stack(
          // crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [_title(), _form(), _submitButton()],
        ),
      ),
    );
  }
}
