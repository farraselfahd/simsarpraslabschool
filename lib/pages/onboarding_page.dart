import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:simsarpraslabschool/pages/login_page.dart';

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({super.key});

  @override
  State<OnboardingPage> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  Widget _logo() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 100),
      child: Align(
        alignment: Alignment.topCenter,
        child: Image.asset(
          "assets/images/logo-1.png",
          height: 300,
          width: 300,
        ),
      ),
    );
  }

  Widget _title() {
    return Container(
      alignment: Alignment.bottomCenter,
      // alignment: Alignment.topCenter,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Text("SILABS",
                textAlign: TextAlign.left,
                style: GoogleFonts.poppins(
                    fontSize: 32, fontWeight: FontWeight.w600)),
          ),
          Align(
              alignment: Alignment.center,
              child: Text(
                "Sistem  Informasi Sarpras BPS Labschool",
                textAlign: TextAlign.left,
                style: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF686777)),
              ))
        ],
      ),
    );
  }

  Widget _button(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
          // width: 328,
          width: double.infinity,
          height: 56,
          child: ElevatedButton(
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ))),
            onPressed: (() {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginPage()));
            }),
            child: Text(
              "Masuk",
              style: GoogleFonts.poppins(
                  fontSize: 16, fontWeight: FontWeight.w500),
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Stack(
          children: [_logo(), _title(), _button(context)],
        ),
      ),
    );
  }
}
